#!/bin/bash

# Requires snmpwalk installed.
# "-c public" must be changed to whatever the community id is.

ADDRESS=$1                      # address given as argument in command
OID=1.3.6.1.2.1.4.22.1.2        # OID for check
REM=iso.3.6.1.2.1.4.22.1.2.3    # Num to remove to clean output
REM2="Authoritative answers can be found from:"

echo executing on... $ADDRESS
sleep .5

com() {
    out=$(exec snmpwalk -v2c -c public $ADDRESS $OID || exec snmpwalk v1 -c pub $ADDRESS $OID) && out=${out//$REM/} && out=${out// /} 
    # command with output set as variable
    macout=$(echo "$out" | cut -f2 -d":"")
    mapfile -t macs < <(echo $macout) # reads list
    echo $ADDRESS
    for i in ${macs[*]} # loop for echoing output
    do
            :
            echo $i
    done
}

com

# This script is using the Simple Network Management Protocol (SNMP) to retrieve the MAC addresses of devices connected to a network. The script takes an IP address as an argument and then uses the snmpwalk command to retrieve the MAC addresses of all devices connected to that IP address. The MAC addresses are then printed out one by one.

# may struggle in environments where devices are used which dont respond to that OID.
